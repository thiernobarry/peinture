<?php

namespace App\Command;

use App\Service\ContactService;
use Symfony\Component\Mime\Email;
use App\Repository\UserRepository;
use Symfony\Component\Mime\Address;
use App\Repository\ContactRepository;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SendContactCommand extends Command
{
    private $userRepository;
    private $mailer;
    private $contactService;
    private $contactRepo;
    protected static $defaultName = 'App:send-contact';

    public function __construct(
        ContactRepository $contactRepo,
        UserRepository $userRepository,
        ContactService $contactService,
        MailerInterface $mailer
    ) {
        $this->contactService = $contactService;
        $this->mailer = $mailer;
        $this->contactRepo = $contactRepo;
        $this->userRepository = $userRepository;
        parent::__construct();
    }

    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ) {
        $toSend = $this->contactRepo->findBy(['estEnvoye' => false]);
        $peintre = $this->contactService->getUser();
        $address = new Address($peintre->getEmail(), $peintre->getNom() . ' ' . $peintre->getPrenom());

        foreach ($toSend as $contact) {
            $email = (new Email())
                    ->from($contact->getEmail())
                    ->to($address)
                    ->subject('Nouveau message de ' . $contact->getNom() . '')
                    ->text($contact->getMessage());
            $this->mailer->send($email);
            $this->contactService->envoie($contact);
        }

        return Command::SUCCESS;
    }
}
