<?php

namespace App\Twig;

use Twig\TwigFunction;
use Twig\Extension\AbstractExtension;
use App\Repository\CategorieRepository;

class AppExtension extends AbstractExtension
{
    private $categorieRepo;

    public function __construct(CategorieRepository $categorieRepo)
    {
        $this->categorieRepo = $categorieRepo;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('categories', [$this, 'getCategories'])
        ];
    }

    /**
     * Permet de récupèrer toutes les catégories
     *
     * @return array
     */
    public function getCategories()
    {
        return $this->categorieRepo->findAll();
    }
}
