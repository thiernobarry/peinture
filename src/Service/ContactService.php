<?php

namespace App\Service;

use DateTime;
use Exception;
use App\Entity\User;
use App\Entity\Contact;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

class ContactService
{
    private $manager;
    private $flash;
    private $userRepo;

    public function __construct(
        EntityManagerInterface $manager,
        FlashBagInterface $flash,
        UserRepository $userRepo
    ) {
        $this->manager = $manager;
        $this->flash = $flash;
        $this->userRepo = $userRepo;
    }

    /**
     * Permet de sauvegarder un contact dans la BD
     *
     * @param Contact $contact
     * @return void
     */
    public function peristContact(Contact $contact): void
    {
        $contact->setEstEnvoye(false)
                ->setCreatedAt(new DateTime('now'));
        $this->manager->persist($contact);
        $this->manager->flush();
        $nom = $this->getUser()->getFullName();
        $this->flash->add('success', "Votre message a été envoyé à $nom");
    }

    /**
     * Permet de récupèrer un utilisateur
     *
     * @return User
     */
    public function getUser(): User
    {
        $user = $this->userRepo->getUser();
        if ($user == null) {
            throw new Exception("Aucun peintre n'a été trouvé");
        }

        return $user;
    }

    public function envoie(Contact $contact)
    {
        $contact->setEstEnvoye(true);

        $this->manager->persist($contact);
        $this->manager->flush();
    }
}
