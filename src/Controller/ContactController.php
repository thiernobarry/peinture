<?php

namespace App\Controller;

use Exception;
use App\Entity\Contact;
use App\Form\ContactType;
use App\Service\ContactService;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ContactController extends AbstractController
{
    /**
     * @Route("/contact", name="me_contacter")
     */
    public function contact(
        Request $request,
        EntityManagerInterface $manager,
        UserRepository $userRepo,
        ContactService $contactService
    ): Response {
        $contact = new Contact();
        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $contactService->peristContact($contact);
        }

        return $this->render(
            'contact/contact.html.twig',
            [
            'form' => $form->createView()
            ]
        );
    }
}
