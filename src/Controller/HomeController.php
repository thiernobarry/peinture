<?php

namespace App\Controller;

use App\Repository\BlogPostRepository;
use App\Repository\PeintureRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(
        PeintureRepository $peintureRepository,
        BlogPostRepository $blogpostRepository
    ): Response {
        $troisDernieresPeintures = $peintureRepository->recherche3PremiersArticles();
        $troisDerniersBlogposts = $blogpostRepository->recherche3PremiersArticles();

        return $this->render('home/index.html.twig', [
            'troisDernieresPeintures' => $troisDernieresPeintures,
            'troisDerniersBlogposts'  => $troisDerniersBlogposts
        ]);
    }
}
