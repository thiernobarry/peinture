<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AproposController extends AbstractController
{
    /**
     * @Route("/apropos", name="apropos")
     */
    public function aPropos(UserRepository $userRepo): Response
    {
        return $this->render(
            'apropos/apropos.html.twig',
            [
             'peintre' => $userRepo->getUser()
            ]
        );
    }
}
