<?php

namespace App\Controller;

use App\Entity\BlogPost;
use App\Repository\BlogPostRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BlogpostController extends AbstractController
{
    /**
     * @Route("/actualites", name="actualites")
     */
    public function actualite(
        BlogPostRepository $blogpostRepository,
        Request $request,
        PaginatorInterface $paginator
    ): Response {
        $blogposts = $blogpostRepository->findAll();
        $pagination = $paginator->paginate(
            $blogposts,
            $request->query->getInt('page', 1),
            6
        );

        return $this->render('blogpost/actualites.html.twig', [
            'blogposts' => $pagination,
        ]);
    }

    /**
      * permet d'afficher les détails d'un blog post
      * @Route("/actualite/{slug}", name="detail_blogpost")
      *
      * @param BlogPost $blogPost
      * @return Response
      */
    public function detail(BlogPost $blogpost): Response
    {
        return $this->render(
            'blogpost/detail.html.twig',
            ['blogpost' => $blogpost]
        );
    }
}
