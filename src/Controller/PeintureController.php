<?php

namespace App\Controller;

use App\Entity\Peinture;
use App\Repository\PeintureRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PeintureController extends AbstractController
{
    /**
     * @Route("/realisation", name="realisations")
     */
    public function realisation(
        PeintureRepository $peintureRepo,
        PaginatorInterface $paginator,
        Request $request,
        EntityManagerInterface $em
    ): Response {
        $dql = 'SELECT p FROM App:Peinture p';
        $query = $em->createQuery($dql);

        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            6
        );

        return $this->render('peinture/realisations.html.twig', [
            'peintures' => $pagination,
        ]);
    }

    /**
     * permet d'afficher les détails d'une peinture
     * @Route("/realisation/{slug}", name="detail_realisation")
     *
     * @param Peinture $peinture
     * @return Response
     */
    public function detail(Peinture $peinture): Response
    {
        return $this->render(
            'peinture/detail.html.twig',
            ['peinture' => $peinture]
        );
    }
}
