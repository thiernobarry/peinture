<?php

namespace App\Controller;

use App\Entity\Peinture;
use App\Entity\Categorie;
use App\Repository\CategorieRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class PortFolioController extends AbstractController
{
    /**
     * Permet de lister toutes les categories
     *
     * @Route("/portfolio", name="port_folio")
     */
    public function listeCategories(CategorieRepository $categorieRepository): Response
    {
        return $this->render('port_folio/portfolio.html.twig', [
            'portfolios' => $categorieRepository->findAll(),
        ]);
    }

    /**
     * permet de lister toutes les peintures d'une catégorie
     * @Route("/portfolio/{slug}", name="peintures_categorie")
     *
     * @return Response
     */
    public function listePeinturesCategorie(
        Categorie $categorie,
        PaginatorInterface $paginator,
        Request $request,
        EntityManagerInterface $em
    ) {
        $peintures = $em->getRepository(Peinture::class)
                         ->findAllPeintures($categorie);
        $pagination = $paginator->paginate($peintures, $request->query->getInt('page', 1), 6);

        return $this->render(
            'port_folio/liste_peintures_categorie.html.twig',
            [
            'peintures' => $pagination,
            'categorie' => $categorie
            ]
        );
    }

    // protected function render(string $view, array $parameters = [], Response $reponse = null): Response
    // {
    //     $parameters['navBarCategorie'] = $this->getDoctrine()->getManager()
    //                                                          ->getRepository(Categorie::class)
    //                                                          ->findAll();

    //     return parent::render($view, $parameters, $reponse);
    // }
}
