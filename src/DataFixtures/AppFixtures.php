<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\User;
use App\Entity\BlogPost;
use App\Entity\Peinture;
use App\Entity\Categorie;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @codeCoverageIgnore
 */
class AppFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');
        $user = new User();
        $user->setNom($faker->firstName())
             ->setPrenom($faker->lastName())
             ->setTelephone($faker->e164PhoneNumber())
             ->setEmail('use@test.com')
             ->setInstagram('instagram')
             ->setPassword($this->encoder->encodePassword($user, 'password'))
             ->setAPropos($faker->text())
             ->setRoles(['ROLE_PEINTRE']);
        $manager->persist($user);
        for ($i = 0; $i < 10; $i++) {
            $blogpost = new BlogPost();
            $blogpost->setTitre($faker->words(3, true))
                     ->setContenu($faker->text())
                     ->setSlug($faker->slug())
                     ->setCreateAt($faker->dateTime())
                     ->setUser($user);
            $manager->persist($blogpost);
        }
        // blog post pour les tests
        $blogpost = new BlogPost();
        $blogpost->setTitre('blogpost titre')
                     ->setContenu($faker->text())
                     ->setSlug('blogpost-titre')
                     ->setCreateAt($faker->dateTime())
                     ->setUser($user);
        $manager->persist($blogpost);

        for ($i = 0; $i < 5; $i++) {
            $categorie = new Categorie();
            $categorie->setNom($faker->name())
                      ->setDescription($faker->text())
                      ->setSlug($faker->slug());

            for ($j = 0; $j < 2; $j++) {
                $peinture = new Peinture();
                $peinture->setNom($faker->name())
                          ->setLargeur($faker->randomFloat(2, 20, 60))
                          ->setHauteur($faker->randomFloat(2, 20, 60))
                          ->setEnVente($faker->boolean())
                          ->setPrix($faker->randomFloat(2, 100000, 1000000))
                          ->setDateRealisation($faker->dateTime())
                          ->setDate($faker->dateTime())
                          ->setDescription($faker->text())
                          ->setPortfolio($faker->boolean())
                          ->setSlug($faker->slug())
                          ->setFile($faker->imageUrl(360, 360, 'animals'))
                          ->setUser($user)
                          ->addCategorie($categorie);
                $manager->persist($peinture);
            }
            $manager->persist($categorie);
        }
        // pour les tests
        $categorie = new Categorie();
        $categorie->setNom('categorie titre')
                      ->setDescription($faker->text())
                      ->setSlug('categorie-titre');

        $peinture = new Peinture();
        $peinture->setNom('peinture titre')
                          ->setLargeur($faker->randomFloat(2, 20, 60))
                          ->setHauteur($faker->randomFloat(2, 20, 60))
                          ->setEnVente($faker->boolean())
                          ->setPrix($faker->randomFloat(2, 100000, 1000000))
                          ->setDateRealisation($faker->dateTime())
                          ->setDate($faker->dateTime())
                          ->setDescription($faker->text())
                          ->setPortfolio($faker->boolean())
                          ->setSlug('peinture-titre')
                          ->setFile($faker->imageUrl(360, 360, 'animals'))
                          ->setUser($user)
                          ->addCategorie($categorie);
        $manager->persist($peinture);

        $manager->persist($categorie);

        $manager->flush();
    }
}
