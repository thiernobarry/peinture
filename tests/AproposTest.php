<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AproposTest extends WebTestCase
{
    public function testShouldDisplayApropos(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/apropos');
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'A propos de moi');
    }
}
