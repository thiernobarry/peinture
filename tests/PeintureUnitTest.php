<?php

namespace App\Tests;

use DateTime;
use App\Entity\User;
use App\Entity\Peinture;
use App\Entity\Categorie;
use App\Entity\Commentaire;
use PHPUnit\Framework\TestCase;

class PeintureUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $datetime = new DateTime();
        $categorie = new Categorie();
        $user = new User();

        $peinture = (new Peinture())
               ->setNom('nom')
               ->setLargeur(12.00)
               ->setHauteur(12.00)
               ->setEnVente(false)
               ->setPrix(12.00)
               ->setDateRealisation($datetime)
               ->setDate($datetime)
               ->setDescription('description')
               ->setPortFolio(true)
               ->setSlug('slug')
               ->setFile('file')
               ->setUser($user)
               ->addCategorie($categorie);

        $this->assertTrue($peinture->getNom() === 'nom');
        $this->assertTrue($peinture->getLargeur() == 12.00);
        $this->assertTrue($peinture->getHauteur() == 12.00);
        $this->assertTrue($peinture->getEnVente() === false);
        $this->assertTrue($peinture->getPrix() == 12.00);
        $this->assertTrue($peinture->getDateRealisation() === $datetime);
        $this->assertTrue($peinture->getDate() === $datetime);
        $this->assertTrue($peinture->getDescription() === 'description');
        $this->assertTrue($peinture->getPortFolio() === true);
        $this->assertTrue($peinture->getSlug() === 'slug');
        $this->assertTrue($peinture->getUser() === $user);
        $this->assertContains($categorie, $peinture->getCategorie());
        $this->assertTrue($peinture->getFile() === 'file');
    }

    public function testIsFalse(): void
    {
        $datetime = new DateTime();
        $categorie = new Categorie();
        $user = new User();

        $peinture = (new Peinture())
               ->setNom('nom')
               ->setLargeur(12.00)
               ->setHauteur(12.00)
               ->setEnVente(false)
               ->setPrix(12.00)
               ->setDateRealisation($datetime)
               ->setDate($datetime)
               ->setDescription('description')
               ->setPortFolio(true)
               ->setSlug('slug')
               ->setFile('file')
               ->setUser($user)
               ->addCategorie($categorie);

        $this->assertFalse($peinture->getNom() === 'nom1');
        $this->assertFalse($peinture->getLargeur() == 120.00);
        $this->assertFalse($peinture->getHauteur() == 120.00);
        $this->assertFalse($peinture->getEnVente() === true);
        $this->assertFalse($peinture->getPrix() == 120.00);
        $this->assertFalse($peinture->getDateRealisation() === new DateTime());
        $this->assertFalse($peinture->getDate() === new DateTime());
        $this->assertFalse($peinture->getDescription() === 'false');
        $this->assertFalse($peinture->getPortFolio() === false);
        $this->assertFalse($peinture->getSlug() === 'false');
        $this->assertFalse($peinture->getUser() === new User());
        $this->assertNotContains(new Categorie(), $peinture->getCategorie());
        $this->assertFalse($peinture->getFile() === 'false');
    }

    public function testIsEmpty(): void
    {
        $peinture = new Peinture();

        $this->assertEmpty($peinture->getNom());
        $this->assertEmpty($peinture->getLargeur());
        $this->assertEmpty($peinture->getHauteur());
        $this->assertEmpty($peinture->getEnVente());
        $this->assertEmpty($peinture->getPrix());
        $this->assertEmpty($peinture->getDateRealisation());
        $this->assertEmpty($peinture->getDate());
        $this->assertEmpty($peinture->getDescription());
        $this->assertEmpty($peinture->getPortFolio());
        $this->assertEmpty($peinture->getSlug());
        $this->assertEmpty($peinture->getFile());
        $this->assertEmpty($peinture->getUser());
        $this->assertEmpty($peinture->getCategorie());
        $this->assertEmpty($peinture->getId());
    }

    public function testRemoveAddCommentaire()
    {
        $commentaire = new Commentaire();
        $peinture = new Peinture();

        $this->assertEmpty($peinture->getCommentaires());

        $peinture->addCommentaire($commentaire);
        $this->assertContains($commentaire, $peinture->getCommentaires());

        $peinture->removeCommentaire($commentaire);
        $this->assertNotContains($commentaire, $peinture->getCommentaires());
    }

    public function testRemoveAddCategorie()
    {
        $peinture = new Peinture();
        $categorie = new Categorie();

        $this->assertEmpty($peinture->getCategorie());
        $peinture->addCategorie($categorie);
        $this->assertContains($categorie, $peinture->getCategorie());

        $peinture->removeCategorie($categorie);
        $this->assertNotContains($categorie, $peinture->getCategorie());
    }
}
