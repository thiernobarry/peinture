<?php

namespace App\Tests;

use App\Entity\Peinture;
use App\Entity\Categorie;
use PHPUnit\Framework\TestCase;

class CategorieUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $categorie = new Categorie();
        $categorie->setNom('cate 1')
                  ->setDescription('description 1')
                  ->setSlug('cate1');

        $this->assertTrue($categorie->getNom() === 'cate 1');
        $this->assertTrue($categorie->getDescription() === 'description 1');
        $this->assertTrue($categorie->getSlug() === 'cate1');
    }

    public function testIsFalse(): void
    {
        $categorie = new Categorie();
        $categorie->setNom('cate 1')
                  ->setDescription('description 1')
                  ->setSlug('cate1');

        $this->assertFalse($categorie->getNom() === 'false');
        $this->assertFalse($categorie->getDescription() === 'description 2');
        $this->assertFalse($categorie->getSlug() === 'cate2');
    }

    public function testIsEmpty():void
    {
        $categorie = new Categorie();

        $this->assertEmpty($categorie->getNom());
        $this->assertEmpty($categorie->getDescription());
        $this->assertEmpty($categorie->getSlug());
        $this->assertEmpty($categorie->getId());
    }

    public function testRemoveAddPeinture()
    {
        $categorie = new Categorie();
        $peinture = new Peinture();

        $this->assertEmpty($categorie->getPeintures());

        $categorie->addPeinture($peinture);
        $this->assertContains($peinture, $categorie->getPeintures());

        $categorie->removePeinture($peinture);
        $this->assertNotContains($peinture, $categorie->getPeintures());
    }
}
