<?php

namespace App\tests;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class HomeUnitTest extends WebTestCase
{
    public function testPageAccueil(): void
    {
        $client = static::createClient();
        $client->request(Request::METHOD_GET, '/');
        $this->assertResponseIsSuccessful();

        //$this->assertResponseStatusCodeSame(Response::HTTP_OK);
        $this->assertSelectorTextContains('h5', 'Site de disgn');
    }
}
