<?php

namespace App\test;

use DateTime;
use App\Entity\Contact;
use PHPUnit\Framework\TestCase;

class ContactUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $contact = new Contact();
        $date = new DateTime();

        $contact->setNom('barry')
        ->setPrenom('thierno')
        ->setEmail('th.barry@yahoo.com')
        ->setEstEnvoye(false)
        ->setMessage('un message')
        ->setCreatedAt($date);
        $this->assertTrue($contact->getNom() === 'barry');
        $this->assertTrue($contact->getPrenom() === 'thierno');
        $this->assertTrue($contact->getEmail() === 'th.barry@yahoo.com');
        $this->assertTrue($contact->getEstEnvoye() === false);
        $this->assertTrue($contact->getMessage() === 'un message');
        $this->assertTrue($contact->getCreatedAt() === $date);
    }

    public function testIsFalse(): void
    {
        $contact = new Contact();
        $date = new DateTime();

        $contact->setNom('barry')
        ->setPrenom('thierno')
        ->setEmail('th.barry@yahoo.com')
        ->setEstEnvoye(false)
        ->setMessage('un message')
        ->setCreatedAt($date);
        $this->assertFalse($contact->getNom() === 'false');
        $this->assertFalse($contact->getPrenom() === 'false');
        $this->assertFalse($contact->getEmail() === 'false');
        $this->assertFalse($contact->getEstEnvoye() === true);
        $this->assertFalse($contact->getMessage() === 'un');
        $this->assertFalse($contact->getCreatedAt() === new DateTime());
    }

    public function testIsEmpty(): void
    {
        $contact = new Contact();
        $this->assertEmpty($contact->getNom());
        $this->assertEmpty($contact->getPrenom());
        $this->assertEmpty($contact->getEmail());
        $this->assertEmpty($contact->getEstEnvoye());
        $this->assertEmpty($contact->getMessage());
        $this->assertEmpty($contact->getCreatedAt());
        $this->assertEmpty($contact->getId());
    }
}
