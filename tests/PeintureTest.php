<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PeintureTest extends WebTestCase
{
    public function testPeinturesDisplay(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/realisation');
        $this->assertResponseIsSuccessful();

        $this->assertSelectorTextContains('h3', 'Mes réalisations');
        //$this->assertPageTitleSame('titre', 'Toutes les peintures');
    }

    public function testPeintureDisplay(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/realisation/peinture-titre');
        $this->assertResponseIsSuccessful();

        $this->assertSelectorTextContains('h5', 'peinture titre');
        //$this->assertPageTitleSame('titre', 'Toutes les peintures');
    }
}
