<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SecurityTest extends WebTestCase
{
    public function testDisplayLogin(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/login');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h3', 'Formulaire de connexion');
    }

    public function testIsLoginSuccess(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/login');

        // select the button
        $buttonCrawlerNode = $crawler->selectButton('submit');

        // retrieve the Form object for the form belonging to this button
        $form = $buttonCrawlerNode->form();

        // optionally, you can combine the last 2 steps by passing an array of
        // field values while submitting the form:
        $client->submit($form, [
            'email'                        => 'use@test.com',
            'password'                     => 'password',
        ]);
        $crawler = $client->request('GET', '/');
        $this->assertResponseIsSuccessful();
        //$this->assertSelectorTextContains('div', 'you are logged in as use@test.com');
    }
}
