<?php

namespace App\Tests;

use DateTime;
use App\Entity\User;
use App\Entity\BlogPost;
use App\Entity\Commentaire;
use PHPUnit\Framework\TestCase;

class BlogPostUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $blogPost = new BlogPost();
        $datetime = new DateTime();
        $user = new User();

        $blogPost->setTitre('titre')
                 ->setContenu('contenu')
                 ->setCreateAt($datetime)
                 ->setSlug('slug')
                 ->setUser($user);

        $this->assertTrue($blogPost->getTitre() === 'titre');
        $this->assertTrue($blogPost->getContenu() === 'contenu');
        $this->assertTrue($blogPost->getCreateAt() === $datetime);
        $this->assertTrue($blogPost->getSlug() === 'slug');
    }

    public function testIsFalse(): void
    {
        $blogPost = new BlogPost();
        $datetime = new DateTime();
        $user = new User();

        $blogPost->setTitre('titre')
                 ->setContenu('contenu')
                 ->setCreateAt($datetime)
                 ->setSlug('slug');
        $this->assertFalse($blogPost->getId() == 2);
        $this->assertFalse($blogPost->getTitre() === 'false');
        $this->assertFalse($blogPost->getContenu() === 'false');
        $this->assertFalse($blogPost->getCreateAt() === new DateTime());
        $this->assertFalse($blogPost->getSlug() === 'false');
        $this->assertFalse($blogPost->getUser() === $user);
    }

    public function testIsEmptyObject(): void
    {
        $blogPost = new BlogPost();

        $this->assertEmpty($blogPost->getId());
        $this->assertEmpty($blogPost->getTitre());
        $this->assertEmpty($blogPost->getContenu());
        $this->assertEmpty($blogPost->getCreateAt());
        $this->assertEmpty($blogPost->getSlug());
        $this->assertEmpty($blogPost->getUser());
    }

    public function testRemoveAddCommentaire()
    {
        $commentaire = new Commentaire();
        $blogPost = new BlogPost();

        $this->assertEmpty($blogPost->getCommentaires());

        $blogPost->addCommentaire($commentaire);
        $this->assertContains($commentaire, $blogPost->getCommentaires());

        $blogPost->removeCommentaire($commentaire);
        $this->assertNotContains($commentaire, $blogPost->getCommentaires());
    }
}
