<?php

namespace App\Tests;

use App\Entity\User;
use App\Entity\BlogPost;
use App\Entity\Peinture;
use PHPUnit\Framework\TestCase;

class UserUnitTest extends TestCase
{
    public function testIsFalse(): void
    {
        $user = new User();
        $user->setNom('barry')
             ->setPrenom('thierno')
             ->setEmail('th.barry@yahoo.com')
             ->setPassword('root')
             ->setInstagram('instagram')
             ->setApropos('apropos')
             ->setRoles(['ROLE_USER'])
             ->setTelephone('02.05.06.06');
        $this->assertTrue($user->getNom() === 'barry');
        $this->assertTrue($user->getPrenom() === 'thierno');
        $this->assertTrue($user->getEmail() === 'th.barry@yahoo.com');
        $this->assertTrue($user->getPassword() === 'root');
        $this->assertTrue($user->getInstagram() === 'instagram');
        $this->assertTrue($user->getApropos() === 'apropos');
        $this->assertTrue($user->getFullName() === 'thierno barry');
        $this->assertTrue($user->getUsername() === 'th.barry@yahoo.com');
        $this->assertTrue($user->getRoles() === ['ROLE_USER']);
        $this->assertTrue($user->getTelephone() === '02.05.06.06');
    }

    public function testIsTrue(): void
    {
        $user = new User();
        $user->setNom('barry')
             ->setPrenom('thierno')
             ->setEmail('th.barry@yahoo.com')
             ->setPassword('root')
             ->setInstagram('instagram')
             ->setApropos('apropos')
             ->setRoles(['ROLE_USER']);

        $this->assertFalse($user->getNom() === 'false');
        $this->assertFalse($user->getPrenom() === 'false');
        $this->assertFalse($user->getEmail() === 'th.barry@gmail.com');
        $this->assertFalse($user->getPassword() === 'raat');
        $this->assertFalse($user->getInstagram() === 'intagram');
        $this->assertFalse($user->getApropos() === 'apropo');
        $this->assertFalse($user->getFullName() === 'barry thierno');
        $this->assertFalse($user->getUsername() === 'thierno');
        $this->assertFalse($user->getRoles() === 'false');
    }

    public function testIsEmpty(): void
    {
        $user = new User();

        $this->assertEmpty($user->getId());
        $this->assertEmpty($user->getNom());
        $this->assertEmpty($user->getPrenom());
        $this->assertEmpty($user->getEmail());
        $this->assertEmpty($user->getPassword());
        $this->assertEmpty($user->getInstagram());
        $this->assertEmpty($user->getApropos());
        $this->assertTrue($user->getFullName() === ' ');
        $this->assertEmpty($user->getUsername());
        $this->assertTrue($user->getRoles() === ['ROLE_USER']);
    }

    public function testRemoveAddPeinture()
    {
        $user = new User();
        $peinture = new Peinture();

        $this->assertEmpty($user->getPeintures());

        $user->addPeinture($peinture);
        $this->assertContains($peinture, $user->getPeintures());

        $user->removePeinture($peinture);
        $this->assertNotContains($peinture, $user->getPeintures());
    }

    public function testRemoveAddBlogPost()
    {
        $user = new User();
        $blogPost = new BlogPost();

        $this->assertEmpty($user->getBlogPosts());

        $user->addBlogPost($blogPost);
        $this->assertContains($blogPost, $user->getBlogPosts());

        $user->removeBlogPost($blogPost);
        $this->assertNotContains($blogPost, $user->getBlogPosts());
    }
}
