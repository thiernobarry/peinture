<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class BlogpostTest extends WebTestCase
{
    public function testActualitesDisplay(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/actualites');
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h3', 'Dernières actualités');
    }

    public function testBlogpostDisplay(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/actualite/blogpost-titre');
        $this->assertResponseIsSuccessful();

        $this->assertSelectorTextContains('h5', 'blogpost titre');
    }
}
