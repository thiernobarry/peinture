<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class PortfolioTest extends WebTestCase
{
    public function testPortfoliosDisplay(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/portfolio');
        $this->assertResponseIsSuccessful();

        $this->assertSelectorTextContains('h1', 'Portfolio');
    }

    public function testPortfolioDisplay(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/portfolio/categorie-titre');
        $this->assertResponseIsSuccessful();

        $this->assertSelectorTextContains('h3', 'Mes réalisations de la catégorie categorie titre');
    }
}
