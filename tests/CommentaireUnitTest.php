<?php

namespace App\Tests;

use DateTime;
use App\Entity\BlogPost;
use App\Entity\Peinture;
use App\Entity\Commentaire;
use PHPUnit\Framework\TestCase;

class CommentaireUnitTest extends TestCase
{
    public function testIsTrue(): void
    {
        $commentaire = new Commentaire();
        $datetime = new DateTime();
        $blogPost = new BlogPost();
        $peiture = new Peinture();

        $commentaire->setAuteur('auteur')
                    ->setEmail('email')
                    ->setCreateAt($datetime)
                    ->setBlogpost($blogPost)
                    ->setContenu('contenu')
                    ->setPeinture($peiture);

        $this->assertTrue($commentaire->getAuteur() === 'auteur');
        $this->assertTrue($commentaire->getEmail() === 'email');
        $this->assertTrue($commentaire->getCreateAt() === $datetime);
        $this->assertTrue($commentaire->getContenu() === 'contenu');
        $this->assertTrue($commentaire->getPeinture() === $peiture);
        $this->assertTrue($commentaire->getBlogpost() === $blogPost);
    }

    public function testIsFalse(): void
    {
        $commentaire = new Commentaire();
        $datetime = new DateTime();
        $blogPost = new BlogPost();
        $peiture = new Peinture();

        $commentaire->setAuteur('auteur')
                    ->setEmail('email')
                    ->setCreateAt($datetime)
                    ->setBlogpost($blogPost)
                    ->setContenu('contenu')
                    ->setPeinture($peiture);

        $this->assertEmpty($commentaire->getAuteur() === 'false');
        $this->assertEmpty($commentaire->getEmail() === 'false');
        $this->assertEmpty($commentaire->getCreateAt() === new DateTime());
        $this->assertEmpty($commentaire->getContenu() === 'false');
        $this->assertEmpty($commentaire->getPeinture() === new Peinture());
        $this->assertEmpty($commentaire->getBlogpost() === new BlogPost());
    }

    public function testIsEmpty(): void
    {
        $commentaire = new Commentaire();

        $this->assertEmpty($commentaire->getAuteur());
        $this->assertEmpty($commentaire->getEmail());
        $this->assertEmpty($commentaire->getCreateAt());
        $this->assertEmpty($commentaire->getContenu());
        $this->assertEmpty($commentaire->getPeinture());
        $this->assertEmpty($commentaire->getBlogpost());
        $this->assertEmpty($commentaire->getId());
    }
}
