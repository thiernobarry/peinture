# peinture

Est un projet symfony permettant de présenter les peintures.

## Environnement de développement 

### Pré-requis

* PHP 7.4
* Composer
* Docker
* Docker-compose

## Lancer l'environnement de développement 

```bash
cd docker
docker-compose up -d
cd ../peintureSymfony
npm install
npm run build
```

# Ajout de l'entité User

```bash
php bin/console make:entity
```

# Faire la modélisation UML
utiliser en ligne LucidChart

https://www.lucidchart.com


# Faire des test

Pour configuer l'ajout des test
```bash
php bin/console make:unit-test
```
Puis lancer les tests


```bash
php bin/phpunit
```

ou pour voir la synthèse

```bash
php bin/phpunit --testdox
```

# Coding standar
suivre le tuto 

https://blog.juansorroche.com/visual-studio-code-configurer-php-cs-fixer

## Ajouter des données de tests

```bash
php bin/console doctrine:fixtures:load
```
## Pour l'envoie des mails

Pour l'envoie des mails de contact en ligne de command
ne pas oublier de mettre en place un cron sur 

```bash
php bin/console app:send-contact
```
## Pour moi
pour créer une classe de test 

```bash
php bin/console make:unit-test
```
Pour lancer le coverage normalement 

```bash
php -n -dzend_extension=xdebug -dxdebug.mode=coverage ./vendor/bin/phpunit --coverage-html var/log/test.html
```
### pour créer une base de données de test
1. copier le fichier .env en .env.test.local
2. changer le nom de la base de données nom_test
3. modifer si necessaire le fichier composer.json  en ajoutant dans la partie script

 ```bash
"prepare-test":[
          "php bin/console doctrine:database:drop --if-exists --force --env=test",
          "php bin/console doctrine:database:create --env=test",
          "php bin/console doctrine:schema:update --force --env=test",
          "php bin/console doctrine:fixture:load -n  --env=test"
        ],
 ```
 Puis lancer la commande
 
 ```bash
composer prepare-test
 ```

